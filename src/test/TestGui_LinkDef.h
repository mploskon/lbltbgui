#ifdef __CINT__

#pragma link off all class;
#pragma link off all function;
#pragma link off all global;
#pragma link off all typedef;

#pragma link C++ class MainFrame;
#pragma link C++ class TimedFrame;
#pragma link C++ class GuiTestFrame;
#pragma link C++ class TabFrame;

#pragma link C++ class FileMonitor;
#pragma link C++ class TextFileMonitor;
#pragma link C++ class BinaryFileMonitor;
#pragma link C++ class FilesMonitor;

#pragma link C++ class BaseMonitorFrame;
#pragma link C++ class GraphicMonitorFrame;
#pragma link C++ class TextMonitorFrame;
#pragma link C++ class DecoderMonitorFrame;

#pragma link C++ class LogFile;

#pragma link C++ class DObject;
#pragma link C++ class DList;
#endif