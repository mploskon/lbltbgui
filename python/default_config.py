#!/usr/bin/env python

import ConfigParser
import sys
import argparse
import os
import datetime
import getpass
import socket


def generate(fname=None, dump=False, write=False):
	config = ConfigParser.ConfigParser()

	now   = datetime.datetime.now()
	snow  = now.strftime("%Y-%m-%d %H:%M:%S")
	suser = getpass.getuser()
	shost = socket.gethostname()

	if fname is None:
		if write:
			fname = 'default_{}_{}_{}.cfg'.format(suser, shost, snow.replace(' ', '_').replace(':', ''))

	config.add_section('info')
	config.set('info', 'date', snow)
	if write:
		config.set('info', 'this_file', os.path.abspath(fname))
	else:
		config.set('info', 'this_file', 'None')
	config.set('info', 'user', suser)
	config.set('info', 'hostname', shost)

	# set a number of parameters
	config.add_section('monitor')
	config.set('monitor', 'input_file', 'default.log')
	config.set('monitor', 'header', '0xC300009')
	config.set('monitor', 'channel', 0)
	config.set('monitor', 'tail', 1000)
	config.set('monitor', 'ignore_file_status', 0)
	config.set('monitor', 'live', 1)
	config.set('monitor', 'missing_trailers', 0)

	if write:
		with open(fname, 'w') as f:
			config.write(f)

	if dump:
		config.write(sys.stdout)


def read_and_dump(fname=None):
	if fname is None:
		fname = 'default.cfg'
	config = ConfigParser.ConfigParser()
	config.read(fname)
	config.write(sys.stdout)


def read_from_file(fname=None):
	if fname is None:
		fname = 'default.cfg'
	config = ConfigParser.ConfigParser()
	config.read(fname)
	return config


def main():
	parser = argparse.ArgumentParser(description='generate/read config files for LBL TB', prog=os.path.basename(__file__))
	# parser.add_argument('-d', '--dump', help = 'dump the contents', action='store_true')
	parser.add_argument('-w', '--write', help='dump the contents', action='store_true')
	parser.add_argument('-f', '--fname', help='file name to operate on', type=str)
	parser.add_argument('-r', '--read', help='read a file', type=str)
	# parser.add_argument('-r', '--read', help = 'read and dump the contents', action='store_true')
	args = parser.parse_args()
	if args.read:
		read_and_dump(args.read)
	else:
		generate(args.fname, dump=True, write=args.write) # args.dump)

if __name__ == '__main__':
	main()
